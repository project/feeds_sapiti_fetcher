<?php

namespace Drupal\feeds_sapiti_fetcher\Feeds\Fetcher\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Feeds\Fetcher\Form\HttpFetcherFeedForm;

/**
 * Provides a form on the feed edit page for the HttpFetcher.
 */
class SapitiFetcherFeedForm extends HttpFetcherFeedForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FeedInterface $feed = NULL) {
    parent::buildConfigurationForm($form, $form_state, $feed);

    $form = parent::buildConfigurationForm($form, $form_state, $feed);

    $form['public_key'] = [
      '#title' => $this->t('Public key:'),
      '#description' => $this->t("Your sapiti's public api key"),
      '#type' => 'textfield',
      '#default_value' => $feed->getConfigurationFor($this->plugin)['public_key'],
    ];
    $form['private_key'] = [
      '#title' => $this->t('Private key:'),
      '#description' => $this->t("Your sapiti's private api key"),
      '#type' => 'textfield',
      '#default_value' => $feed->getConfigurationFor($this->plugin)['private_key'],
    ];
    $form['lang_code'] = [
      '#title' => $this->t('Language code:'),
      '#description' => $this->t("Language code in two lowercase characters."),
      '#type' => 'textfield',
      '#default_value' => $feed->getConfigurationFor($this->plugin)['lang_code'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state, FeedInterface $feed = NULL) {
    parent::submitConfigurationForm($form, $form_state, $feed);
    $feed_config = $feed->getConfigurationFor($this->plugin);
    $feed_config['public_key'] = $form_state->getValue('public_key');
    $feed_config['private_key'] = $form_state->getValue('private_key');
    $feed_config['lang_code'] = $form_state->getValue('lang_code');
    
    $feed->setConfigurationFor($this->plugin, $feed_config);
  }

}
