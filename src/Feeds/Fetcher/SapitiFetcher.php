<?php

namespace Drupal\feeds_sapiti_fetcher\Feeds\Fetcher;

use Drupal\feeds\Exception\EmptyFeedException;
use Drupal\feeds\Feeds\Fetcher\HttpFetcher;
use Drupal\feeds\Result\HttpFetcherResult;
use Drupal\feeds\StateInterface;
use Drupal\feeds\Utility\Feed;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use Symfony\Component\HttpFoundation\Response;
use Drupal\feeds\FeedInterface;

/**
 * Defines an HTTP fetcher.
 *
 * @FeedsFetcher(
 *   id = "sapiti_fetcher",
 *   title = @Translation("Sapiti Fetcher"),
 *   description = @Translation("Get feeds from sapati api using public/private keyx and timestamp."),
 *   form = {
 *     "configuration" = "Drupal\feeds\Feeds\Fetcher\Form\HttpFetcherForm",
 *     "feed" = "Drupal\feeds_sapiti_fetcher\Feeds\Fetcher\Form\SapitiFetcherFeedForm",
 *   }
 * )
 */
class SapitiFetcher extends HTTPFetcher {

  /**
   * {@inheritdoc}
   */
  public function defaultFeedConfiguration() {
    $default_configuration = parent::defaultConfiguration();
    $default_configuration['private_key'] = '';
    $default_configuration['public_key'] = '';
    $default_configuration['lang_code'] = '';
    return $default_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function fetch(FeedInterface $feed, StateInterface $state) {
    $sink = $this->fileSystem->tempnam('temporary://', 'feeds_http_fetcher');
    $sink = $this->fileSystem->realpath($sink);

    // Get cache key if caching is enabled.
    $cache_key = $this->useCache() ? $this->getCacheKey($feed) : FALSE;

    $response = $this->get($feed->getSource(), $sink, $cache_key, $feed->getConfigurationFor($this)['public_key'], $feed->getConfigurationFor($this)['private_key'], $feed->getConfigurationFor($this)['lang_code']);
    // @todo Handle redirects.
    // @codingStandardsIgnoreStart
    // $feed->setSource($response->getEffectiveUrl());
    // @codingStandardsIgnoreEnd

    // 304, nothing to see here.
    if ($response->getStatusCode() == Response::HTTP_NOT_MODIFIED) {
      $state->setMessage($this->t('The feed has not been updated.'));
      throw new EmptyFeedException();
    }

    return new HttpFetcherResult($sink, $response->getHeaders());
  }

  /**
   * Performs a GET request.
   *
   * @param string $url
   *   The URL to GET.
   * @param string $sink
   *   The location where the downloaded content will be saved. This can be a
   *   resource, path or a StreamInterface object.
   * @param string $cache_key
   *   (optional) The cache key to find cached headers. Defaults to false.
   *
   * @return \Guzzle\Http\Message\Response
   *   A Guzzle response.
   *
   * @throws \RuntimeException
   *   Thrown if the GET request failed.
   *
   * @see \GuzzleHttp\RequestOptions
   */
  protected function get($url, $sink, $cache_key = FALSE, $public_key = '', $private_key = '', $lang_code = '') {
    $url = Feed::translateSchemes($url);

    $options = [RequestOptions::SINK => $sink];

    if($public_key !== '' && $private_key !== '') {
      $body = "";

      $timestamp= date('c');
      $stringToHash= $public_key . $timestamp;
      $signature = hash_hmac("sha256", $stringToHash, $private_key);

      $body = [
        "publickey" => $public_key,
        "timestamp" => $timestamp,
        "signature" => $signature,
        "language" => $lang_code,
      ];
      
      $options[RequestOptions::BODY] = json_encode($body);
    }

    // Adding User-Agent header from the default guzzle client config for feeds
    // that require that.
    if (isset($this->client->getConfig('headers')['User-Agent'])) {
      $options[RequestOptions::HEADERS]['User-Agent'] = $this->client->getConfig('headers')['User-Agent'];
    }

    // Add cached headers if requested.
    if ($cache_key && ($cache = $this->cache->get($cache_key))) {
      if (isset($cache->data['etag'])) {
        $options[RequestOptions::HEADERS]['If-None-Match'] = $cache->data['etag'];
      }
      if (isset($cache->data['last-modified'])) {
        $options[RequestOptions::HEADERS]['If-Modified-Since'] = $cache->data['last-modified'];
      }
    }

    try {
      $response = $this->client->getAsync($url, $options)->wait();
    }
    catch (RequestException $e) {
      $args = ['%site' => $url, '%error' => $e->getMessage()];
      throw new \RuntimeException($this->t('The feed from %site seems to be broken because of error "%error".', $args));
    }

    if ($cache_key) {
      $this->cache->set($cache_key, array_change_key_case($response->getHeaders()));
    }

    return $response;
  }

}
